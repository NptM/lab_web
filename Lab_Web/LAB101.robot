*** Settings ***
Library           SeleniumLibrary
Library           BuiltIn
Library           String
Library           Collections
Library           ExcelRobot
Library           Dialogs

*** Test Cases ***
TC02
    Log To Console    ${EMPTY}
    Open Excel    E:\\Robot_Training\\Data\\Data2.xlsx
    ${rowCountInput}    Get Row Count    INPUT
    @{UsernameList}    Create List
    @{pNameList}    Create List
    @{quantityList}    Create List
    @{price}    Create List
    @{resultUsernameList}    Create List
    @{resultPasswordList}    Create List
    @{resultQtyList}    Create List
    @{resultColorList}    Create List
    FOR    ${input}    IN RANGE    1    ${rowCountInput}
        ${tag}    Read Cell Data    INPUT    0    ${input}
        Continue For Loop If    '${tag}' != 'Run'
        ${username}    Read Cell Data    INPUT    1    ${input}
        ${password}    Read Cell Data    INPUT    2    ${input}
        ${qtyList}    Read Cell Data    INPUT    3    ${input}
        ${color}    Read Cell Data    INPUT    4    ${input}
        Append To List    ${resultUsernameList}    ${username}
        Append To List    ${resultPasswordList}    ${password}
        Append To List    ${resultQtyList}    ${qtyList}
        Append To List    ${resultColorList}    ${color}
    END
    ${totalUser}    Get Length    ${resultUsernameList}
    Open Web Advantage
    FOR    ${i}    IN RANGE    0    ${totalUser}
        ${username}    Set Variable    ${resultUsernameList}[${i}]
        ${password}    Set Variable    ${resultPasswordList}[${i}]
        ${qtyList}    Set Variable    ${resultQtyList}[${i}]
        ${Color}    Set Variable    ${resultColorList}[${i}]
        Sleep    2s
        Login    ${username}    ${password}    ${UsernameList}
        ClickTablets
        AddToCart    ${qtyList}    ${Color}
        CheckOut    ${pNameList}    ${quantityList}    ${price}
        CheckPoint
        SignOut
    END
    ${outputlength}    Get Length    ${UsernameList}
    Open Excel To Write    E:\\Robot_Training\\Data\\Data2.xlsx    E:\\Robot_Training\\Data\\Data2Copy.xlsx    override=override
    FOR    ${output}    IN RANGE    0    ${outputlength}
        ${i}    Evaluate    ${output}+2
        Write To Cell By Name    OUTPUT    A${i}    ${UsernameList}[${output}]
        Write To Cell By Name    OUTPUT    B${i}    ${pNameList}[${output}]
        Write To Cell By Name    OUTPUT    C${i}    ${quantityList}[${output}]
        Write To Cell By Name    OUTPUT    D${i}    ${price}[${output}]
        Write To Cell By Name    OUTPUT    E${i}    ${resultColorList}[${output}]
        Write To Cell By Name    OUTPUT    F${i}    ${status}
        Write To Cell By Name    OUTPUT    G${i}    ${msg}
    END
    Close Browser
    Save Excel

TC01
    Log To Console    ${EMPTY}
    Open Excel    E:\\Robot_Training\\Data\\Data.xlsx
    ${columnCountInput}    Get Column Count    INPUT
    ${rowCountInput}    Get Row Count    INPUT
    @{UsernameList}    Create List
    @{CityList}    Create List
    @{resultUsernameList}    Create List
    @{resultPasswordList}    Create List
    @{resultCityList}    Create List
    FOR    ${input}    IN RANGE    1    ${rowCountInput}
        ${username}    Read Cell Data    INPUT    0    ${input}
        ${password}    Read Cell Data    INPUT    1    ${input}
        ${city}    Read Cell Data    INPUT    2    ${input}
        Append To List    ${resultUsernameList}    ${username}
        Append To List    ${resultPasswordList}    ${password}
        Append To List    ${resultCityList}    ${city}
    END
    ${ListLength}    Get Length    ${resultUsernameList}
    FOR    ${i}    IN RANGE    0    ${ListLength}
        ${username}    Set Variable    ${resultUsernameList}[${i}]
        ${password}    Set Variable    ${resultPasswordList}[${i}]
        ${city}    Set Variable    ${resultCityList}[${i}]
        Open Web Advantage
        Sleep    5s
        Login    ${username}    ${password}
        ClickAccount
        ClickEditAccountDetails
        InputCityAndClickSave    ${city}
        Log Out    ${UsernameList}    ${CityList}
    END
    ${outputlength}    Get Length    ${UsernameList}
    Open Excel To Write    E:\\Robot_Training\\Data\\Data.xlsx    E:\\Robot_Training\\Data\\DataCopy.xlsx    override=override
    FOR    ${output}    IN RANGE    0    ${outputlength}
        ${i}    Evaluate    ${output}+2
        Write To Cell By Name    OUTPUT    A${i}    ${UsernameList}[${output}]
        Write To Cell By Name    OUTPUT    B${i}    ${CityList}[${output}]
    END
    Save Excel

How to use Excel
    Set Library Search Order    ExcelRobot
    Open Excel    E:\\Robot_Training\\Data\\TestData1.xlsx
    ${columnCountInput}    Get Column Count    INPUT
    ${rowCountInput}    Get Row Count    INPUT
    ${columnCountOutput}    Get Column Count    OUTPUT
    @{resultUsernameList}    Create List    ${EMPTY}
    @{resultPasswordList}    Create List    ${EMPTY}
    FOR    ${input}    IN RANGE    1    ${rowCountInput}
        ${tag}    Read Cell Data    INPUT    0    ${input}
        Run Keyword If    '${tag}' != 'Run'    Continue For Loop
        ${username}    Read Cell Data    INPUT    1    ${input}
        ${password}    Read Cell Data    INPUT    2    ${input}
        Append To List    ${resultUsernameList}    ${username}
        Append To List    ${resultPasswordList}    ${password}
    END
    Remove From List    ${resultUsernameList}    0
    Remove From List    ${resultPasswordList}    0
    ${rowCountOutput}    Get Length    ${resultUsernameList}
    Open Excel To Write    E:\\Robot_Training\\Data\\TestData1.xlsx    new_path=E:\\Robot_Training\\Data\\TestData1Copy.xlsx    override=override
    FOR    ${output}    IN RANGE    0    ${rowCountOutput}
        ${i}    Evaluate    ${output}+2
        Write To Cell By Name    OUTPUT    A${i}    PASS    data_type=TEXT
        Write To Cell By Name    OUTPUT    B${i}    SUCCESS
        Write To Cell By Name    OUTPUT    C${i}    ${resultUsernameList}[${output}]
        Write To Cell By Name    OUTPUT    D${i}    ${resultPasswordList}[${output}]
    END
    Save Excel

TC_LAB
    Open Web Testit    https://www.youtube.com/    chrome

How to use Evaluate
    ${result1}    Evaluate    1 + 2
    ${result2}    Evaluate    10 - 5
    ${result3}    Evaluate    ${result1} + ${result2}
    ${result4}    Evaluate    ${result2} * ${result3}
    ${result5}    Evaluate    ${result2} / ${result3}
    ${result6}    Evaluate    random.randint(0,99)
    ### Result ###
    Log To Console    ${result1} = 1 + 2
    Log To Console    ${result2} = 10 - 5
    Log To Console    ${result3} = ${result1} + ${result2}
    Log To Console    ${result6}

How to use Convert To
    ${result1}    Convert To Integer    200
    ${result2}    Convert To Binary    210
    ${result3}    Convert To Number    42.6258744    2
    ${result4}    Convert To String    12 String
    ${result5}    Convert To Hex    10
    ### Result ###
    Log To Console    Convert To Integer -> ${result1}
    Log To Console    Convert To Binary -> ${result2}
    Log To Console    Convert To Number -> ${result3}
    Log To Console    Convert To String -> ${result4}
    Log To Console    Convert To Hex-> ${result5}

How to use Should Be Equal
    Should Be Equal    1 THB    1 THB
    Should Be Equal As Numbers    2.5    2.5
    Should Be Equal As Integers    ABCD    abcd    base=16
    Should Be Equal As Integers    10    10
    Should Be Equal As Strings    John    John

How to use Strings
    ${valueStr1}    Catenate    Hello World!!!
    ${valueStr2}    Remove String    Robot Framework    ${SPACE}    Robot
    ${valueStr3}    Split String    Hello World!!!    ${SPACE}
    ${valueStr4}    Replace String    Hello World!!!    ${SPACE}    Fxxking

How to use Collections List
    Log To Console    xxxxx
    @{List}    Create List    B    A    B    3
    Append To List    ${List}    D
    Append To List    ${List}    Z
    Log To Console    ${List}
    ${countVal}    Count Values In List    ${List}    B
    Insert Into List    ${List}    0    C
    Log To Console    ${List}
    Insert Into List    ${List}    3    A
    Log To Console    ${List}
    Sort List    ${List}
    Remove Values From List    ${List}    A
    Log To Console    ${List}

How to use Collections Dictionary
    Log To Console    xxxxx
    &{animal}    BuiltIn.Create Dictionary    Dog=4    Spider=8    Human=2
    ${leg}    Get From Dictionary    ${animal}    Human
    Remove From Dictionary    ${animal}    Human
    Log To Console    ${animal}
    Set To Dictionary    ${animal}    Human=2
    Log To Console    ${animal}
    Set To Dictionary    ${animal}    Cat=4
    Log To Console    ${animal}
    Log Dictionary    ${animal}
    &{header}    BuiltIn.Create Dictionary    Content-Type=application/json    Token= Bearer xxxxxx

How to use Capture Screen
    Open Web Mendix
    Capture Page Screenshot    E:\\Robot_Training\\Lab_Web\\Screen\\img01.png
    Close Browser

How to use For Loop
    @{animalList}    BuiltIn.Create List    Human    Dog    Cat    Spider
    Log To Console    ${EMPTY}
    FOR    ${i}    IN RANGE    0    2
        Log To Console    Loop1 Animal -> ${animalList[${i}]}
    END
    FOR    ${i}    IN    3
        Log To Console    Loop2 Animal -> ${animalList[${i}]}
    END

How to use IF Condition
    ${point}    Set Variable    80
    ${grade}    Set Variable    ${point}>=80    A    B
    BuiltIn.Set Suite Variable    ${Message}    A
    Show Grade

How to use Run Keyword If
    ${point}    Set Variable    40
    Log To Console    Point = ${point}
    Run Keyword If    ${point} >= 80    Log To Console    Grade A
    ...    ELSE IF    80 > ${point} >= 70    Log To Console    Grade B
    ...    ELSE IF    70 > ${point} >= 60    Log To Console    Grade C
    ...    ELSE    Log To Console    Grade D

ATM1
    Log To Console    ${EMPTY}
    ${have}    Evaluate    random.randint(0,999999)
    Log To Console    I have ${have} Bath
    ${balance1}    Evaluate    ${have}%1000
    ${thounson}    Evaluate    (${have} - ${balance1})/1000
    ${thounson1}    Convert To Integer    ${thounson}
    RUN KEYWORD IF    ${thounson1} >= 1    Log To Console    I get bank 1000: ${thounson1} Bank
    ${balance2}    Evaluate    ${balance1}%500
    ${fivehun}    Evaluate    (${balance1} - ${balance2})/500
    ${fivehun1}    Convert To Integer    ${fivehun}
    RUN KEYWORD IF    ${fivehun1} >= 1    Log To Console    I get bank 500: ${fivehun1} Bank
    ${balance3}    Evaluate    ${balance2}%100
    ${onehun}    Evaluate    (${balance2} - ${balance3})/100
    ${onehun1}    Convert To Integer    ${onehun}
    RUN KEYWORD IF    ${onehun1} >= 1    Log To Console    I get bank 100: ${onehun1} Bank
    ${balance4}    Evaluate    ${balance3}%50
    ${fifty}    Evaluate    (${balance3} - ${balance4})/50
    ${fifty1}    Convert To Integer    ${fifty}
    RUN KEYWORD IF    ${fifty1} >= 1    Log To Console    I get bank 50: ${fifty1} Bank
    ${balance5}    Evaluate    ${balance4}%20
    ${twenty}    Evaluate    (${balance4} - ${balance5})/20
    ${twenty1}    Convert To Integer    ${twenty}
    RUN KEYWORD IF    ${twenty1} >= 1    Log To Console    I get bank 20: ${twenty1} Bank
    ${balance6}    Evaluate    ${balance5}%10
    ${ten}    Evaluate    (${balance5} - ${balance6})/10
    ${ten1}    Convert To Integer    ${ten}
    RUN KEYWORD IF    ${ten1} >= 1    Log To Console    I get coin 10: ${ten1} Coin
    ${balance7}    Evaluate    ${balance6}%5
    ${five}    Evaluate    (${balance6} - ${balance7})/5
    ${five1}    Convert To Integer    ${five}
    RUN KEYWORD IF    ${five1} >= 1    Log To Console    I get coin 5: ${five1} Coin
    ${balance8}    Evaluate    ${balance7}%2
    ${two}    Evaluate    (${balance7} - ${balance8})/2
    ${two1}    Convert To Integer    ${two}
    RUN KEYWORD IF    ${two1} >= 1    Log To Console    I get coin 2: ${two1} Coin
    ${balance9}    Evaluate    ${balance8}%1
    ${one}    Evaluate    (${balance8} - ${balance9})/1
    ${one1}    Convert To Integer    ${one}
    RUN KEYWORD IF    ${one1} >= 1    Log To Console    I get coin 1: ${one1} Coin

Quiz01
    [Documentation]    Quiz Test 01
    ...    Programming Test
    ...    If writing a Logic Summary Numbers in this order [1, -3, -5, 0, 4, 5]
    ...    Lets calculate the percentage of the count.
    @{num}    Create List    1    -3    -5    0    4    5    20
    Log To Console    ${EMPTY}
    ${count}    Get Length    ${num}
    @{natural}    Create List
    FOR    ${i}    IN RANGE    ${count}
        Run Keyword If    ${num[${i}]}<=0    Continue For Loop
        Append To List    ${natural}    ${num[${i}]}
    END
    ${ncount}    Get Length    ${natural}
    ${percentage}    Evaluate    (${ncount} /${count} ) * 100
    ${percentage2f}    Convert To Number    ${percentage}    2
    Log To Console    Percentage of Natural number is ${percentage2f} %

Quiz02
    [Documentation]    Quiz Test 02
    ${value}    Get Value From User    Please input X value
    Log To Console    ${EMPTY}
    @{list}    Create List
    FOR    ${i}    IN RANGE    ${value}
        Run Keyword    Draw    ${i}    ${value}    ${list}
    END

Quiz03
    ${value}    Get Value From User    Please input X value (2-1000)
    @{prime}    Create List
    Log To Console    ${EMPTY}
    FOR    ${num}    IN RANGE    2    ${value}+1
        isPrime    ${num}    ${prime}
    END
    ${TotalPrimeNum}    Get Length    ${prime}
    Run Keyword If    ${TotalPrimeNum}>0    Log To Console    Total prime number = ${TotalPrimeNum}
    Run Keyword If    ${TotalPrimeNum}>0    Log To Console    Prime number = ${prime}

Three Cup Monte
    [Documentation]    3 Cups Monte
    ...
    ...    บอลเริ่มต้น อยู่ในแก้วที่ 1 เสมอ
    ...    A คือ สลับแก้วที่ 1 และ 2
    ...    B คือ สลับแก้วที่ 2 และ 3
    ...    C คือ สลับแก้วที่ 1 และ 3
    Log To Console    ${EMPTY}
    ${input}    Get Value From User    Please input A, B or C
    ${upper}    Convert To Upper Case    ${input}
    ${pos}    Set variable    1
    ${status}    Set variable    'OK'
    @{list}    Convert To List    ${upper}
    ${length}    Get Length    ${list}
    FOR    ${i}    IN RANGE    0    ${length}
        BuiltIn.Continue For Loop If    '${list}[${i}]' == 'A' or '${list}[${i}]' == 'B' or '${list}[${i}]' == 'C'
        BuiltIn.Run Keyword If    '${list}[${i}]' != 'A' or '${list}[${i}]' != 'B' or '${list}[${i}]' != 'C'    Set Suite Variable    ${status}    'EXIT'
        BuiltIn.Exit For Loop If    ${status} == 'EXIT'
    END
    Run Keyword If    ${status} == 'OK'    ShuffleCup    ${length}    ${pos}    ${upper}    ${list}
    ...    ELSE    Log To Console    INVALID VALUE!!!

XPath
    Open Web Testit    https://robotframework.org/    chrome
    Log To Console    ${EMPTY}
    ${getText}    Get Text    //*[@id="monaco-container"]//*[contains(text(),'Doc')]
    ${text}    Remove String    ${getText}    ${SPACE}
    Log To Console    Message --> ${text}
    Close Browser

*** Keywords ***
Open Web Testit
    [Arguments]    ${URL}    ${Browser}
    Open Browser    ${URL}    ${Browser}    options=add_experimental_option("detach", True)
    Maximize Browser Window

Open Web Mendix
    Open Browser    https://www.mendix.com    chrome    options=add_experimental_option("detach", True)
    Maximize Browser Window
    Wait Until Page Contains Element    //*[@id="button-top-nav-sign-up"]/span    10s
    ${text}=    Get Text    //*[@id="button-top-nav-sign-up"]/span
    Run Keyword if    '${text}'=='Start for free'    Log To Console    Open Web Mendix Success
    ...    ELSE    Log To Console    Open Web Mendix Success Fail

Show Grade
    Log To Console    ${Message}

Draw
    [Arguments]    ${i}    ${value}    ${list}
    FOR    ${j}    IN RANGE    ${value}
        Run Keyword If    ${j} == ${i}    Append To List    ${list}    X
        Append To List    ${list}    O
    END
    Remove From List    ${list}    ${value}
    Log To Console    ${list}
    Remove Values From List    ${list}    X    O

isPrime
    [Arguments]    ${num}    ${prime}
    ${factor_count}    Set Variable    0
    FOR    ${i}    IN RANGE    2    ${num}
        Run Keyword If    ${factor_count}> 0    Exit For Loop
        Run Keyword If    ${num}%${i} != 0    Continue For Loop
        ${factor_count}    Evaluate    ${factor_count} + 1
    END
    Run Keyword If    ${factor_count} == 0    Append To List    ${prime}    ${num}

ShuffleCup
    [Arguments]    ${length}    ${pos}    ${upper}    ${list}
    FOR    ${index}    IN RANGE    0    ${length}
        Run Keyword If    '${list}[${index}]' == 'A'    ShuffleA    ${pos}
        Run Keyword If    '${list}[${index}]' == 'B'    ShuffleB    ${pos}
        Run Keyword If    '${list}[${index}]' == 'C'    ShuffleC    ${pos}
    END
    Log To Console    Input: ${upper}
    Log To Console    Output: ${pos}

ShuffleA
    [Arguments]    ${pos}
    Run Keyword If    ${pos} == 1    Set Suite Variable    ${pos}    2
    ...    ELSE IF    ${pos} == 2    Set Suite Variable    ${pos}    1

ShuffleB
    [Arguments]    ${pos}
    Run Keyword If    ${pos} == 2    Set Suite Variable    ${pos}    3
    ...    ELSE IF    ${pos} == 3    Set Suite Variable    ${pos}    2

ShuffleC
    [Arguments]    ${pos}
    Run Keyword If    ${pos} == 1    Set Suite Variable    ${pos}    3
    ...    ELSE IF    ${pos} == 3    Set Suite Variable    ${pos}    1

ClickAccount
    Wait Until Page Contains Element    //*[@id="menuUserLink"]
    Click Element    //*[@id="menuUserLink"]
    Click Element    //*[@id="loginMiniTitle"]/label[text()="My account"]
    Wait Until Page Contains Element    //h3[@class="roboto-regular sticky fixedImportant ng-scope"]    10s
    ${text}    Get Text    //h3[@class="roboto-regular sticky fixedImportant ng-scope"]
    Run Keyword If    '${text}' == 'MY ACCOUNT'    Log To Console    Success
    ...    ELSE    Log To Console    Failed

ClickEditAccountDetails
    Sleep    5s
    Click Element    //*[@id="myAccountContainer"]/div[1]/h3/a
    Wait Until Page Contains Element    //*[@id="registerPage"]/article/h3    5s

InputCityAndClickSave
    [Arguments]    ${city}
    Sleep    2s
    Input Text    //*[@name="cityAccountDetails"]    ${city}
    Click Button    //*[@id="save_btnundefined"]
    Wait Until Page Contains Element    //h3[text()='MY ACCOUNT']    10s
    ${text}    Get Text    //*[@id="myAccountContainer"]/div[1]/div/div[2]/label[2]
    Run Keyword If    '${text}' == '${city}'    Log To Console    Success
    ...    ELSE    Log To Console    Failed

Log Out
    [Arguments]    ${UsernameList}    ${CityList}
    ${utext}    Get Text    //span[contains(@class,'hi-user con')]
    ${ctext}    Get Text    //*[@id="myAccountContainer"]/div[1]/div/div[2]/label[2]
    Append To List    ${UsernameList}    ${utext}
    Append To List    ${CityList}    ${ctext}
    Click Element    //*[@id="menuUserLink"]
    ${text}    Get Text    //*[@id="loginMiniTitle"]/label[3]
    Run Keyword If    '${text}' == 'Sign out'    Click Element    //*[@id="loginMiniTitle"]/label[3]
    Close Browser

Open Web Advantage
    Open Browser    http://advantageonlineshopping.com/    chrome    options=add_experimental_option("detach", True)
    Maximize Browser Window
    Wait Until Page Contains Element    //*[@id="menuUser"]    10s
    Click Element    //*[@id="menuUser"]

Login
    [Arguments]    ${username}    ${password}    ${UsernameList}
    Wait Until Page Contains Element    //*[@id="sign_in_btnundefined"]    20s
    Input Text    //*[@name="username"]    ${username}
    Input Text    //*[@name="password"]    ${password}
    Sleep    1s
    Click Element    //*[@id="sign_in_btnundefined"]
    Sleep    5s
    ${user}    Get Text    //*[@id="menuUserLink"]/span
    Run Keyword if    '${username}' == '${user}'    Log To Console    ${username} -->Login Success
    ...    ELSE    Log To Console    -- Login Fail --
    Run Keyword if    '${username}' == '${user}'    Append To List    ${UsernameList}    ${username}

ClickTablets
    Wait Until Page Contains Element    //*[@id="tabletsImg"]    10s
    Sleep    1s
    Click Element    //*[@id="tabletsImg"]
    Wait Until Page Contains Element    //h3[@class="categoryTitle roboto-regular sticky ng-binding"]    10s
    ${text}    Get Text    //h3[@class="categoryTitle roboto-regular sticky ng-binding"]
    Run Keyword If    '${text}' == 'TABLETS'    Log To Console    -- Displayed Catagory Tablets --
    ...    ELSE    Log To Console    -- Display Fail --
    Click Element    //img[@id="16"]

AddToCart
    [Arguments]    ${qtyList}    ${Color}
    Wait Until Page Contains Element    //button[@name="save_to_cart"]
    Run Keyword If    '${Color}' == 'GRAY'    Click Element    //*[@id="rabbit"][@title="GRAY"]
    FOR    ${index}    IN RANGE    1    ${qtyList}
        Click Element    //*[@class="plus"]
    END
    Click Element    //button[@name="save_to_cart"]
    Sleep    2s

CheckOut
    [Arguments]    ${pNameList}    ${quantityList}    ${price}
    Click Element    //*[@id="shoppingCartLink"]
    Wait Until Page Contains Element    //h3[@class="roboto-regular center sticky fixedImportant ng-binding"]
    ${name}    Get Text    //label[@class="roboto-regular productName ng-binding"]
    ${qty}    Get Text    //*[@class="smollCell quantityMobile"]//*[@class="ng-binding"]
    ${tprice}    Get Text    //*[@class="smollCell"]//*[@class="price roboto-regular ng-binding"]
    ${totalprice}    Remove String    ${tprice}    $
    Append To List    ${pNameList}    ${name}
    Append To List    ${quantityList}    ${qty}
    Append To List    ${price}    ${totalprice}
    Click Element    //*[@class="remove red ng-scope"]
    Wait Until Page Contains Element    //*[@id="shoppingCart"]/div/label

CheckPoint
    ${text}    Get Text    //*[@id="shoppingCart"]/div/label
    Run Keyword If    '${text}' == 'Your shopping cart is empty'    Set Suite Variable    ${status}    PASS
    ...    ELSE    Set Suite Variable    ${status}    FAILED
    Run Keyword If    '${status}' == 'PASS'    Set Suite Variable    ${msg}    Check Out Successfully
    ...    ELSE    Set Suite Variable    ${msg}    Check Out Fail
    Log To Console    Status --> ${status}
    Log To Console    Message --> ${msg}

SignOut
    Click Element    //*[@id="menuUserLink"]
    ${text}    Get Text    //*[@id="loginMiniTitle"]/label[text()="Sign out"]
    Run Keyword If    '${text}' == 'Sign out'    Click Element    //*[@id="loginMiniTitle"]/label[text()="Sign out"]
    Sleep    2s
    Click Element    //*[@id="shoppingCart"]/div/a
    Wait Until Page Contains Element    //*[@id="menuUser"]    10s
    Click Element    //*[@id="menuUser"]
